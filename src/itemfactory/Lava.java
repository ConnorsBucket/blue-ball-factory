package itemfactory;

import java.awt.geom.Rectangle2D;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 *
 * Lava pit destroys the ball when it comes in contact with it.
 * 
 * deletes the ball from the array as well as permanantly interrupting the corresponding thread. Currently has a problem where it cannot delete heaps of balls at one time.
 * 
 * @author Konnor
 */
public class Lava extends Machine {
        /**
        *  set coordinates
        * @param x x coordinate of the lava
        * @param y y coordinate of the lava
        * @param XSIZE length of the lava
        * @param YSIZE height of the lava
         */
    public Lava(int x, int y, int XSIZE,int YSIZE)
    {
        super(x, y, XSIZE, YSIZE);
    }
    /**
    * @return returns true if the ball comes in contact with the lava
    * 
    * 
    */
    public Boolean checkInteraction(Ball ball) 
    {
       if(ball.getShape().getMinY()==getShape().getMinY()
               &&ball.getShape().getMinX()>getShape().getMinX()
               &&ball.getShape().getMaxX()<getShape().getMaxX()) 
                
                {
            
                return true;
            
        }
        return false;
    }
    
    /**
    * Interrupts the Thread. My way of "killing" the thread
    * 
    * @param ball ball to be moved
    */
    public void moveBall(Ball ball)
    {
        
       
        Thread.currentThread().interrupt();
        
        
      
    }
    
    
    
    
}
