/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itemfactory;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Graphics2D;
import java.awt.event.ActionListener;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;


/**
 *      Factory Frame is the frame in which all the machines and balls are displayed in. 
 * 
 * @author Konnor
 */
public class FactoryFrame extends JFrame {      // creates the window adds buttons and events
    private FactoryComponents comp;
    /**
    * number of steps to be taken by the ball before stopping
    * 
    */
    public static final int STEPS = 1000;
    /**
    * Delay between each movement of the ball
    * 
    */
    public static final int DELAY = 5;
    
    /**
    * 
    * Frame in which all the balls are displayed in
    * 
    */
    
    public FactoryFrame()
    {
        setTitle("Item Factory");
        comp=new FactoryComponents();
        add(comp, BorderLayout.CENTER);
        JPanel buttonPanel=new JPanel();
        addButton(buttonPanel, "Release Ball", event -> addBall());
        addButton(buttonPanel, "Close", event -> System.exit(0));
        add(buttonPanel, BorderLayout.SOUTH);
        pack();
    }
   
    /**
    * adds buttons to the window of the factory
    * 
    * @param c container
    * @param title title of the frame
    * @param listener listener
    * 
    */
                

    public void addButton(Container c, String title, ActionListener listener)
    {
        JButton button = new JButton(title);
        c.add(button);
        button.addActionListener(listener);
    }
    /**
    * adds a ball to the factory. Each ball creates a new thread, and each time it runs though it checks the interaction between all the machines before 
    * moving in the current direction 
    * 
    */
    public void addBall()
    {
           
            Lock lock = new ReentrantLock();
            Ball ball = new Ball(15,15,279,0);
            comp.addBall(ball);
            Runnable r = () -> {
                try
            {
            for(int i=1; i <= STEPS; i++)
            {
                
            while(!Thread.currentThread().isInterrupted()) {
                ball.moving=true;
                comp.checkInteraction(ball);
                comp.rotorInteraction(ball);
                ball.move(comp.getBounds());
                
                comp.repaint();
                Thread.sleep(DELAY);
            }
            
        }
            }
        catch(InterruptedException e)
        {
            comp.removeBall(ball); //removes ball from the array when it is interrupted. 
        }
        };
            Thread t = new Thread(r);
            t.start();
    }
    /**
    *   adds the rotors to the the factory
    *       
    * 
    */ 
   public void addRotor(){
        PolyRotor myRotor =  new PolyRotor();
        PolyRotor myRotor1 = new PolyRotor();
        PolyRotor myRotor2 = new PolyRotor();
        PolyRotor myRotor3 = new PolyRotor();
        myRotor.PolyRotor(0.195,286, 75);
        myRotor1.PolyRotor(-0.785, 0, 0);  // rotates the rototr 45 degrees 
        myRotor2.PolyRotor(0.195,312, 125);
        myRotor3.PolyRotor(-0.195,213, 125);
        
        
        
        comp.addRotor((PolyRotor)myRotor);
        comp.addRotor((PolyRotor)myRotor1);
        comp.addRotor((PolyRotor)myRotor2);
        comp.addRotor((PolyRotor)myRotor3);
        myRotor1.rotate(myRotor1.PolyRotor());
        myRotor1.PolyRotor(-0.195, 238, 75);
        
        Runnable r = () -> {
                try
            {
            for(int i=1; i <= 100000000; i++)
            {
                
                myRotor.rotate(myRotor.PolyRotor());
                myRotor1.rotate(myRotor1.PolyRotor());
                myRotor2.rotate(myRotor2.PolyRotor());
                myRotor3.rotate(myRotor3.PolyRotor());
               
                
                comp.repaint();
                
                
                Thread.sleep(100);
            }
        }
        catch(InterruptedException e)
        {
        }
        };
            Thread t = new Thread(r);
            t.start();
            
    }
    /**
    * Creates and adds all the machines to the factory
    * 
    */
    public void newMachine()
    {
            
            Wall firstWall=new Wall(450,200,15,200);
            Wall secondWall=new Wall(350,320, 15, 100);
            Wall thirdWall=new Wall(50, 320, 15, 100);
            Wall fourthWall=new Wall(200, 450, 15, 100);
            
            Treadmill leftTreadMill=new Treadmill(160,300,100,15, "left");
            Treadmill rightTreadMill=new Treadmill(320,300,100,15, "right");
            Treadmill anotherTreadMill=new Treadmill(410,450,200,15, "right");
            
            Cannon cannon=new Cannon(560,400,50,50);
            Lava lava=new Lava(0,600,1000,15);
            Dispensor dispensor=new Dispensor(260,0,50,50);
          
            comp.addMachine((Machine)dispensor);
            comp.addMachine((Machine)firstWall);
            comp.addMachine((Machine)secondWall);
            comp.addMachine((Machine)thirdWall);
            comp.addMachine((Machine)fourthWall);
            
            comp.addMachine((Machine)leftTreadMill);
            comp.addMachine((Machine)rightTreadMill);
            comp.addMachine((Machine)anotherTreadMill);
            
            comp.addMachine((Machine)cannon);
            comp.addMachine((Machine)lava);
            
           
    }
    


}
