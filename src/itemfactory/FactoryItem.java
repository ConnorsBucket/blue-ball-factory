/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itemfactory;

import java.awt.geom.Rectangle2D;
import java.awt.geom.RectangularShape;

/**
 *
 * 
 * 
 * Factory Item is the super class all the items in the factory inherit from
 * @author Konnor
 */
public abstract class FactoryItem {
    /**
        *  Horizontal size of the factory Item
         */
    public int XSIZE;
    /**
        *  vertical size of the factory Item
         */
    public int YSIZE;
    /**
        *  starting x coordinate size of the factory Item
         */
    public double x;
    /**
        *  starting x coordinate size of the factory Item
         */
    public double y;
    
    /**
 * @param XSIZE Horizontal size of the factory Item
 * @param YSIZE Vertical size of the factory Item
 * @param x intial x coordinate of the factory Item
 * @param y Initial y coordinate of the factory Item
 * 
 */
    public FactoryItem(int XSIZE, int YSIZE, int x, int y)
            
    {
         
        this.XSIZE=XSIZE;
        
        this.YSIZE=YSIZE;
        
        this.x=x;
        
        this.y=y;
    }
         /**
        *  @return Rectangular shape in the shape of the ball 
         */
    public abstract RectangularShape getShape();
  
    
}
