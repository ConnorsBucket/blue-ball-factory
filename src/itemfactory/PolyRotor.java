/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itemfactory;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Paint;
import java.awt.Polygon;
import java.awt.geom.AffineTransform;
import java.awt.geom.Line2D;
import java.awt.geom.PathIterator;
import java.awt.geom.Point2D;
import java.awt.geom.Rectangle2D;
import javafx.scene.shape.Shape;

/**
 * polyrotor sends the ball to the right or left, depending on its rotation
 * @author MSE-6
 */
public class PolyRotor{
    //Polygon P;
   
    private int[] xdot = {0,25  ,25,25  ,50,25  ,25,25};
    private int[] ydot = {25,25  ,0,25  ,25,25  ,50,25};
    private double xCenter = 0;
    private double yCenter = 0;
    private double rotato =0;
    private int activeX1 =0;
    private int activeX2 =0;
    private int activeY1 = 0;
    private int activeY2 = 0;
    private boolean antiClockWise;

    /**
     *
     * @param rotation angle in Radians that the rotor rotates in each time rotate is called
     * @param xpos x postion of the rotor
     * @param ypos y postion of the rotor 
     */
    public Polygon PolyRotor(double rotation, int xpos, int ypos){
        
        Polygon P = new Polygon();
        
        P.xpoints = xdot;
       
        P.ypoints = ydot;
        P.npoints = xdot.length;
        
            activeX1 = xpos;
            activeY1 = ypos;
            
            rotato = rotation;
            xCenter = (xpos + xdot[1]);
            
            yCenter = (ypos + ydot[1]);
            activeX2 = (xpos + (int)xCenter);
          
            activeY2 = (ypos + (int)yCenter);
            if(rotation < 0){
                antiClockWise = true;
            }
            else{this.antiClockWise = false;}
            
             for(int i =0; i< xdot.length; i++){
            xdot[i] = (xdot[i] + xpos);
            ydot[i] = (ydot[i] + ypos);
             }
            
   
        return P;
        
        
       }
    
    /**
     *
     * @return returns the Rotor polygon 
     */
    public Polygon PolyRotor(){
        
        Polygon P = new Polygon();
        
        P.xpoints = xdot;
       
        P.ypoints = ydot;
        P.npoints = xdot.length;
    
        return P;
        
        
       }
    
    /**
     *
     * @param rotation
     * @param xpos
     * @param ypos
     */

        
    /**
     *
     * @return returns the x coordinates for the polygon
     */
    public int[] getXdot(){
            return xdot;
        }

    /**
     *
     * @return returns the y coordinates for the polygon
     */
    public int[] getYdot(){
            return ydot;
        }
        
    /**
     *
     * @param ball checks if ball is touching then moves the ball acorrdingly 
     */
    public void cheakInteraction(Ball ball){
      
        Line2D line1 = new Line2D.Double(xdot[0],ydot[0],xdot[1], ydot[1]);
        Line2D line2 = new Line2D.Double(xdot[2],ydot[2],xdot[3], ydot[3]);
        Line2D line3 = new Line2D.Double(xdot[4],ydot[4],xdot[5], ydot[5]);
        Line2D line4 = new Line2D.Double(xdot[6],ydot[6],xdot[7], ydot[7]);
        
        int x1 = activeX1;
        int x2 = (int)xCenter;
        int y1 = activeY1;
        int y2 = (int)yCenter;
        
        int ax1 = (int)xCenter;
        int ax2 = (activeX1 + 50);
        int ay1 = activeY1;
        int ay2 = (int)yCenter;
                
        if(antiClockWise){
            
                 if((ax1 - ((int)(ball.getXSIZE()/2)))  > ball.getShape().getCenterX() 
                         && ball.getShape().getCenterX() > x1 
                         && y1 < ball.getShape().getCenterY() 
                         && ay2 > ball.getShape().getCenterY()){
          
               ball.down();
               //System.out.println(xCenter);
               }
         
        if( line1.intersects(ball.getShape().getBounds()) == true)
        {
           System.out.println("line1"); 
           ball.rotating = true;
           
           
           if(ax1 < ball.getShape().getCenterX() 
                   && ball.getShape().getCenterX() < ax2 
                   && ay1 < ball.getShape().getCenterY() 
                   && ay2 > ball.getShape().getCenterY()){
               
           ball.left();
           }
         
                
        }
         if( line2.intersects(ball.getShape().getBounds()) == true)
        {
            //System.out.println("line2"); 
            ball.rotating = true;
             if(ax1 < ball.getShape().getCenterX() && ball.getShape().getCenterX() < ax2 && ay1 < ball.getShape().getCenterY() && ay2 > ball.getShape().getCenterY()){
           ball.left();
           }
        }
          if( line3.intersects(ball.getShape().getBounds()) == true  )
        {
            //System.out.println("line3"); 
            ball.rotating = true;
             if(ax1 < ball.getShape().getCenterX() && ball.getShape().getCenterX() < ax2 && ay1 < ball.getShape().getCenterY() && ay2 > ball.getShape().getCenterY()){
           ball.left();
           }
        }
           if( line4.intersects(ball.getShape().getBounds()) == true)
        {
            //System.out.println("line4"); 
            ball.rotating = true;
             if(ax1 < ball.getShape().getCenterX() && ball.getShape().getCenterX() < ax2 && ay1 < ball.getShape().getCenterY() && ay2 > ball.getShape().getCenterY()){
           ball.left();
           }
        }
           
        }    
        
        ////////
        if(!antiClockWise){
        if((xCenter + (ball.getXSIZE()/2)) < ball.getShape().getCenterX() && ball.getShape().getCenterX() < ax2 && y1 < ball.getShape().getCenterY() && y2 > ball.getShape().getCenterY()){
       
               ball.down();
               //System.out.println(xCenter);
               }
         
        if( line1.intersects(ball.getShape().getBounds()) == true)
        {
           System.out.println("line1"); 
           ball.rotating = true;
           
           
           if(x1 < ball.getShape().getCenterX() 
                   && ball.getShape().getCenterX() < x2 
                   && y1 < ball.getShape().getCenterY() 
                   && y2 > ball.getShape().getCenterY()){
               
           ball.right();
           }
         
                
        }
         if( line2.intersects(ball.getShape().getBounds()) == true)
        {
            //System.out.println("line2"); 
            ball.rotating = true;
             if(x1 < ball.getShape().getCenterX() && ball.getShape().getCenterX() < x2 && y1 < ball.getShape().getCenterY() && y2 > ball.getShape().getCenterY()){
           ball.right();
           }
        }
          if( line3.intersects(ball.getShape().getBounds()) == true  )
        {
            //System.out.println("line3"); 
            ball.rotating = true;
             if(x1 < ball.getShape().getCenterX() && ball.getShape().getCenterX() < x2 && y1 < ball.getShape().getCenterY() && y2 > ball.getShape().getCenterY()){
           ball.right();
           }
        }
           if( line4.intersects(ball.getShape().getBounds()) == true)
        {
            //System.out.println("line4"); 
            ball.rotating = true;
             if(x1 < ball.getShape().getCenterX() && ball.getShape().getCenterX() < x2 && y1 < ball.getShape().getCenterY() && y2 > ball.getShape().getCenterY()){
           ball.right();
           }
        }
        }
           ball.rotating = false;
         }
         
    /**
     *
     * @param p requires a polyRotor Polygon to rotate
     */
    public void rotate(Polygon p) {
  if (rotato == 0) {};
  AffineTransform rotation = AffineTransform.getRotateInstance(rotato, xCenter, yCenter);
  PathIterator pit = p.getPathIterator(rotation);
  float[] a = new float[6]; // as per PathIterator.currentSegment() spec
  Polygon rp = new Polygon();
  int ty;
  do {
    ty = pit.currentSegment(a);
    if (ty != PathIterator.SEG_CLOSE)
      rp.addPoint(Math.round(a[0]), Math.round(a[1]));
    pit.next();
  } while (ty!=PathIterator.SEG_CLOSE && !pit.isDone());
  xdot = rp.xpoints;
  ydot = rp.ypoints;
  }
}
