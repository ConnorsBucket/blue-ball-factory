/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itemfactory;

import java.awt.geom.Rectangle2D;

/**
 *  Rectangle that the ball will bounce off of when it comes in contact with it. The ball can touch  the wall from any angle and will change its course depending on this angle.
 * @author Konnor
 */
public class Wall extends Machine {
   
    /**
    * @param XSIZE length of the wall
    * @param YSIZE height of the wall
    * @param x x coordinate of the wall
    * @param y y coordinate of the wall
    *
    */
    public Wall(int XSIZE, int YSIZE, int x,int y)
    {
        super(XSIZE, YSIZE, x,y);
    }
    /**
    *
    * Checks if the ball needs to bounce off of the wall. 
    * 
    * @return returns true if ball is touching the wall
    */
    
    public Boolean checkInteraction(Ball ball) 
  {
        if(ball.getShape().getMaxX()==getShape().getMinX() 
                && ball.getShape().getMaxY() <= getShape().getMaxY()
                && ball.getShape().getMinY() >= getShape().getMinY()
            ||ball.getShape().getMinX()==getShape().getMaxX()
                    && ball.getShape().getMaxY() <= getShape().getMaxY()
                       && ball.getShape().getMinY() >= getShape().getMinY())
               
                
        {
            
                return true;
            
        }
        return false;
  }
    /**
    *
    * Moves the ball depending on what angle it is on when it touches the wall. 
    * 
    */
    public void moveBall(Ball ball)
    {
        if(ball.getShape().getMaxX()==getShape().getMinX()          //If it is touching the right side of the wall 
                && ball.getShape().getMaxY() <= getShape().getMaxY()
                && ball.getShape().getMinY() >= getShape().getMinY())
                
                {
                    if(ball.getDirection()=="rightDown")
                    {
                        ball.leftDown();
                    }
                    if(ball.getDirection()=="rightUp"||ball.getDirection()=="rightUpBoost")
                    {
                        ball.leftUp();
                    }
                   
                    if(ball.getDirection()=="left")
                    {
                        ball.right();
                    }
        
                }
        if(ball.getShape().getMinX()==getShape().getMaxX()
                    && ball.getShape().getMaxY() <= getShape().getMaxY()
                       && ball.getShape().getMinY() >= getShape().getMinY())
        {
           if(ball.getDirection()=="leftDown")
                    {
                        ball.rightDown();
                    }
                    if(ball.getDirection()=="leftUp")
                    {
                        ball.rightUp();
                    }
                    
                    if(ball.getDirection()=="left")
                    {
                        ball.right();
                    }
        }
    }
 
        
        
        
    
    
    
    
  }
    

