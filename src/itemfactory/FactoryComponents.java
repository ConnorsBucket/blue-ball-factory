/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


/**
 * 
 * @author Konnor
 */
package itemfactory;
import static com.sun.org.apache.bcel.internal.Repository.instanceOf;
import java.awt.*;
import java.awt.geom.AffineTransform;
import java.awt.geom.RectangularShape;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import javax.swing.*;
import static com.sun.org.apache.bcel.internal.Repository.instanceOf;
/**
 *  Factory Components contains all the components of the factory
 * @author Konnor
 */
public class FactoryComponents extends JPanel {
    private static final int DEFAULT_WIDTH = 1000;
    private static final int DEFAULT_HEIGHT = 900;
    
    private java.util.List<Ball> balls = new ArrayList<>();
    private java.util.List<Machine> machines = new ArrayList<>();
    private java.util.List<PolyRotor> rotors = new ArrayList<>();
    
    

    /**
    * 
    * Gets all the machines in the factory
    * @return Array of machines
    * 
    * 
    */
     
     public ArrayList<Machine> getMachines()
     {
         return (ArrayList<Machine>) machines;
     }
     /**
    * 
    * gets all the rotors in the rotors array
    * 
    * @return array of rotors
    * 
    */
     
     public ArrayList<PolyRotor> getRotors(){
         return (ArrayList<PolyRotor>) rotors;
     }
     /**
    *
    *       gets all the rotors in the factory
    *       @param ball ball to interact with rotor
    */ 
     public void rotorInteraction(Ball ball){
         for(PolyRotor r : rotors ){
             ball.lockBall();
             r.cheakInteraction(ball);
             ball.unlockBall();
             
         }
     }
     /**
    *
    *       Removes a ball from the balls array
    *       Cannot remove heaps at one time
    *       @param ball ball to be removed
    * 
    */ 
     public void removeBall(Ball ball)
     {
         
         balls.remove(ball);
         
}

    //Ball.unlockBall();
     /**
    *
    *       Checks if the ball needs to interact with any of the machines in the factory.
    *       If it does, both the ball and machine are locked while the machine uses the ball.
    *       
    *       @param ball to be checked against the all the machines
    */ 
     public void checkInteraction(Ball ball)
     {
         for(Machine machine : machines)
        {
         if(machine.checkInteraction(ball))
         {
             
                ball.lockBall();
                machine.lockMachine();
                machine.moveBall(ball); 
                machine.unlockMachine();//ball can only be moved by 1 machine at a time
                ball.unlockBall();
         }
               
                
           }
        }
     /**
    *
    *       Adds rotor to the rotors array
    *       @param b polyrotor to be added
    */ 
     public void addRotor(PolyRotor b){
         
         
           rotors.add(b);
         
     }


       /**
    *
    *       adds a ball to the ball array
    *       @param b ball to be added to the array
    */       
    public void addBall(Ball b)   //
    {
            
            balls.add(b);
        
        
            
        
        
        
    }
    /**
    *
    *       Paints all the items in the factory, displaying them on the screen
    *       changes colour of the machine depending on what it is
    * 
    */ 
   public void paintComponent(Graphics g)
   {
        super.paintComponent(g);
        Graphics2D g2 = (Graphics2D) g;
        java.util.List<Shape> shapes = new ArrayList<>();

     
        for(Ball b : balls)
        {
           
            g2.setColor(Color.BLUE);
             
            g2.fill(b.getShape());
            
            
        }
        
        
        
        for(PolyRotor b : rotors)
        {
            
            
            g2.setColor(Color.red);
            g2.setStroke(new BasicStroke(4));
            g2.draw(b.PolyRotor());
            
            g2.setColor(Color.black);
            
            
        }
        
        
        
        for(Machine b : machines)
        {
            if(b instanceof Cannon)
            {
                Image img1 = Toolkit.getDefaultToolkit().getImage("Cannon.png");
                int x=(int)b.getShape().getX();
                int y=(int)b.getShape().getY();
                        
                g2.drawImage(img1, x,y, this);
                g2.finalize();
                //g2.setColor(Color.ORANGE);
            }
            else
            {
            if(b instanceof Dispensor)
            {
                g2.setColor(Color.MAGENTA);
            }
            else if(b instanceof Treadmill)
            {
                g2.setColor(Color.GRAY);
                
                
                
            }
            else if(b instanceof Lava)
            {
                g2.setColor(Color.red);
            }
            
            else
            {
                g2.setColor(Color.BLACK);
            }
            g2.fill(b.getShape());
            }
            
        }
   }


/**
    *
    *       Adds a machine to the machines array
    *       
    *       @param b to be added to the array
    */ 
    public void addMachine(Machine b)
    {
       
        machines.add(b);
        
        
    }
  
/**
    *
    *       sets the size of the window
    *       
    *       @return Dimension prefered size of the dimension
    */ 
        
        
    
   
    public Dimension getPreferredSize(){return new Dimension(DEFAULT_WIDTH, DEFAULT_HEIGHT);}
}

