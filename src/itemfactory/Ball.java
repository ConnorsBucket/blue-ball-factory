/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itemfactory;

import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RectangularShape;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Ball is the item that will be navigating though the factory. 
 * @author Konnor
 */
public class Ball extends FactoryItem{ 
    
    /**
    * lock associated with the ball 
    */
    public Lock ballLock=new ReentrantLock();
    private double dx=0;
    private double dy=1;
    
    
    Boolean moving=false;
    String previousDirection="";
    String direction="";
    Boolean rotating = false;
    /**
    * @param x intial xcoordinate of the ball 
    * @param y initial y coordinate of the ball
    * @param XSIZE width of the ball 
    * @param YSIZE height of the ball 
    */
    public Ball(int x, int y, int XSIZE, int YSIZE)
    {
        super(x, y, XSIZE,YSIZE);
    }
    /**
    * sets the x and y coordinates of the ball
    * 
    * @param x xCoordinate of the ball 
    * @param y yCoordinate of the ball
    * 
    * 
    */
    public void XY(int x, int y)
    {
        this.x=x;
        this.y=y;
    }
   /**
    * 
    * 
    * @return gets the width of the ball
    * 
    */
    public int getXSIZE(){
        return XSIZE;
    }
    /**
    * 
    * 
    * @return gets the height of the ball
    * 
    */
    public int getYSIZE(){
        return YSIZE;
    }
    /**
    * 
    * 
    * @return gets the direction in which the ball is currently travelling
    * 
    */
    public String getDirection()
    {
        return direction;
    }
    /**
    * 
    * 
    * @return returns the lock associated with the ball 
    */
    public Lock getLock()
    {
        return ballLock;
    }
    /**
    * locks the ball
    * 
    */
    public void lockBall()
    {
        ballLock.lock();
    }
    /**
    * unlocks the ball
    * 
    */
    public void unlockBall()
    {
        ballLock.unlock();
    }
    /**
    * 
    * 
    * sets the direction of the ball to left
    * 
    */
    public void left(){
        previousDirection = direction;
        this.lockBall();
        direction = "left";
    }
    
    /**
    * changes the direction of the ball to up
    * 
    */
    public void up()
    {
        dy=-dy;
        direction="up";
        
    }
    /**
    * changes the direction of the ball to down
    * 
    */
    public void down()
    {
        direction="down";
    }
    /**
    * changes the direction of the ball to move left up
    * 
    */
    public void leftUp()
    {
        
        this.lockBall();
        direction="leftUp";
    }
    /**
    * changes the direction of the ball to right up
    * 
    */
    public void rightUp()
    {
        
        this.lockBall();
        direction="rightUp";
    }
    /**
    * changes the direction of the ball to left down
    * 
    */
    public void leftDown()
    {
       
        direction="leftDown";
         previousDirection=direction;
    }
    /**
    * changes the direction of the ball to right down
    * 
    */
    public void rightDown()
    {
        
        direction="rightDown";
         previousDirection=direction;
    }
    /**
    * makes the ball go in a right up direction, but at a faster speed
    * 
    */
   public void rightUpBoost()
   {
       direction="rightUpBoost";
   }
   /**
    * changes the direction of the ball to right
    * 
    */
    public void right()
    {
        //moving=false;
        previousDirection=direction;
        this.lockBall();
        direction="right";
        
    }
    
    /**
    * sets the direction of the ball to nothing
    * 
    */
    
    public void nothing()
    {
        direction="nothing";
    }
    
        
    /**
    * moves the ball a step in its current direction
    * 
    * @param bounds the bounds in which the ball is contained in
    */
    public void move(Rectangle2D bounds)  // the ball is actually a rectangle ...no it isn't
    {
        
       
        if(direction=="right")
        {
            
            dy = 0;
            dx = 1;
            
        }
            
        
        if(direction=="rightDown")
        {
            dx=1;
            dy=1;
        }
             
         if(direction=="leftDown")
         {
             dy=1;
            dx=-1;
         }
         if(direction=="down")
         {
             dx=0;
             dy=1;
         }
         if(direction=="leftUp")
         {
             this.lockBall();
             dy=-1;
             dx=-1;
         }
         if(direction=="rightUp")
         {
             dy=-1;
             dx=1;
         }
         if(direction=="rightUpBoost")
         {
             dy=-1;
             dx=2;
         }
         if(direction=="left"){
             dy = 0;
             dx = -1;
         }
         
         
            
        x+=dx;
        y+=dy;
        
        //this.unlockBall();
        

        
        if(x < bounds.getMinX())//left side of the screen
        {
            
            //x=bounds.getMinX(); probs not needed add back in if it breaks
            if(direction=="left")
            {
                direction="right";
            }
            if(direction=="leftUp")
            {
                direction="rightUp";
            }
            if(direction=="leftDown")
            {
                direction="rightDown";
            }
            
            
        }
        if(x+XSIZE >= bounds.getMaxX())//right side of the screen
        {

            if(direction=="right")
            {
                direction="left";
            }
            if(direction=="rightUp"||direction=="rightUpBoost")
            {
                direction="leftUp";
            }
            if(direction=="rightDown")
            {
                direction="leftDown";
            }
        }
        if(y+YSIZE < bounds.getMinY())//top of the screen
        {
            if(direction=="leftUp")
            {
                direction="leftDown";
            }
            if(direction=="up")
            {
                direction="down";
            }
            if(direction=="rightUp")
            {
                direction="rightDown";
            }
           
        }
        if(y + YSIZE >= bounds.getMaxY())//bottom of the screen
        {

        
            if(direction=="rightDown")
            {
                direction="rightUp";
            }
            if(direction=="down")
            {
                direction="up";
            }
            if(direction=="leftDown")
            {
                direction="leftUp";
            }
        }
    }
    /**
    * @return returns a RectangluarShape, in the shape of the ball
    * 
    */
    public RectangularShape getShape() 
            {
                
                return new Ellipse2D.Double(x,y,XSIZE,YSIZE);
            }
}


