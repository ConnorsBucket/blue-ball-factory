/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itemfactory;

import java.awt.geom.Rectangle2D;
import java.util.concurrent.locks.Condition;
/**
    * 
* Treadmill sends the ball to the right when it is on it. After reaching the end of the treadmill, the ball will move in a right down direction
 * 
 n direction that the ball will go in when it is on the treadmill. can only be left or right
 * @author Konnor
 */
public class Treadmill extends Machine{
    
    String direction;
   /**
    * 
 * Treadmill sends the ball to the right when it is on it. After reaching the end of the treadmill, the ball will move in a right down direction
 * 
 * @param x x coordinate of the treadmill
 * @param y y coordinate of the treadmill
 * @param XSIZE length of the treadmill
 * @param YSIZE height of the treadmill
 * @param direction direction that the ball will go in when it is on the treadmill. can only be left or right
 * 
 */
    public Treadmill(int x, int y, int XSIZE,int YSIZE, String direction)
    {
        super(x, y, XSIZE,YSIZE);
        this.direction=direction;
    }
    /**
     * 
    * @return returns true if the ball is touching the top of the treadmill
    */
    public Boolean checkInteraction(Ball ball)
  {
      if(ball.getShape().getMaxY()==getShape().getMinY()&&ball.getShape().getMinX()<=getShape().getMaxX()&&ball.getShape().getMinX()>=getShape().getMinX())
      {
          return true;
          
           
      }
      
      return false;
  }
      
     /**
    * moves the ball in the direction of the set location
    * 
    */
    
  
    public void moveBall(Ball ball)
    {
        if(direction=="right")
        {
        if(ball.getShape().getMaxX()==getShape().getMaxX())
        {
            ball.rightDown();
        }
        else
        {
        ball.right();
        }
        }
        if(direction=="left")
            if(ball.getShape().getMinX()==getShape().getMinX())
        {
            ball.leftDown();
        }
        else
        {
        ball.left();
        }
    }
  
}
