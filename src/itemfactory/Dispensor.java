/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itemfactory;

import java.awt.geom.Rectangle2D;

/**
 *Dispensor is where the balls are dropped from. The location of the dispenser will be what the coordinates of the ball is initially set to. Also decides what direction the ball is initially going in
 * 
 * 
 * @author Konnor
 */
public class Dispensor extends Machine {
    /**
    * 
    * set the coordinates and size of the Dispenser
    * 
    * @param x initial x coordinate of the dispenser
    * @param y initial y coordinate of the dispenser
    * @param XSIZE width of the dispenser
    * @param YSIZE height of the dispenser
    * 
    * 
    */
    
    public Dispensor(int x, int y, int XSIZE, int YSIZE)
    {
        super(x, y, XSIZE,YSIZE);
    }
    
    /**
    * Checks if the ball is moving. 
    * 
    * @return returns true or false. false if it is moving, true if it isn't
    * 
    */
    
    public Boolean checkInteraction(Ball ball)
    {
        //return false;
        if(ball.moving)
        {
        return false;
        }
        else
        {
        return true;//square that it can bounce off
        }
    }   
    
    /**
    * moves the ball in a downward direction, starting from the location of the dispenser. Used to initially drop the ball when it is added to the factory
    * 
    */
    public void moveBall(Ball ball)
    {
        ball.XY((int)x,(int)y);
        ball.down();
    }
    
    
    
}
