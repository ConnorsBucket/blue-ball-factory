/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itemfactory;

import java.awt.geom.Rectangle2D;
import java.util.Random;

/**
 * Cannon receives a ball and fires it out in right up direction, at a faster speed
 * @author Konnor
 */
public class Cannon extends Machine {
    /**
    * 
    * set the coordinates and size of the cannon
    * 
    * @param x initial x coordinate of the cannon
    * @param y initial y coordinate of the cannon
    * @param XSIZE width of the cannon
    * @param YSIZE height of the cannon
    * 
    * 
    */
    
   public Cannon(int x, int y, int XSIZE,int YSIZE)
   {
       super(x, y, XSIZE,YSIZE);
   }
    
    
    public Boolean checkInteraction(Ball ball)
  {
        if(ball.getShape().getMinX()==getShape().getMinX() 
                &&ball.getShape().getMaxX()>=getShape().getMinX()
                && ball.getShape().getMaxY() <= getShape().getMaxY()
                && ball.getShape().getMinY() >= getShape().getMinY())
                {
            
                return true;
            
        }
        return false;
  }
    /**
    * after a brief pause, the Cannon Fires the ball out into a right up position, at a slightly faster speed
    * 
    */
    public void moveBall(Ball ball)
    {
        
        
        
        try
        {
            
        Thread.sleep(1000);
        ball.rightUpBoost();
        
        
        
        }
        catch(InterruptedException e)
        {
            
        }
        
        
    }
  
}
