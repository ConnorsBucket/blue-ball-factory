/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
    @author Konnor
 */

package itemfactory;

import java.awt.geom.Ellipse2D;
import java.awt.geom.Rectangle2D;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 *
 * 
 * 
 * Machine is the super class that all the machines inherit from. 
 * 
 * 
 * @author Konnor
 * 
 */
public abstract class Machine extends FactoryItem {
    /**
     *  unique Lock of the factory Item 
     */
     public Lock machineLock =new ReentrantLock();
     /**
     *  @param x x coordinate of the machine
     *  @param y y coordinate of the machine
     *  @param XSIZE length of the machine
     *  @param YSIZE height of the machine
     */
     public Machine(int x, int y, int XSIZE,int YSIZE)
     {
         super(x, y, XSIZE,YSIZE);
     }
     /**
    *
    * Locks the machine
    * 
    */
     public void lockMachine()
     {
         machineLock.lock();
     }
     /**
    *
    * unlocks the machine
    * 
    */
     public void unlockMachine()
     {
         machineLock.unlock();
     }
   
     /**
    *
    * checkInteraction checks if the ball is interacting with the machine(usually if it enters the vicinity). If it needs to interact it will return true.
    * 
    * @param ball the ball to be checked against the machine, to see if it interacts
    * @return returns true if the ball comes in contact with the machine
    * 
    */
    public abstract Boolean checkInteraction(Ball ball);
    /**
    *
    * moves the ball in the way the machine wants it to.
    * 
    * @param ball the ball to be moved by the machine
    * 
    */
    public abstract void moveBall(Ball ball);
    
    /**
    * returns Rectangle2D of the shape of the machine
    * 
    * @return returns the shape of the object
    */
    public Rectangle2D getShape()
    {
        return new Rectangle2D.Double(XSIZE, YSIZE, x, y);
    }
            
}


    
    
            
    




