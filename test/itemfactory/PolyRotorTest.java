/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itemfactory;

import java.awt.Polygon;
import java.awt.geom.Ellipse2D;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author MSE-6
 */
public class PolyRotorTest {
    
    private PolyRotor rotor;
    
    
    public PolyRotorTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
        rotor = new PolyRotor();
        
    }
    
    @After
    public void tearDown() {
        rotor = null;
    }

    /**
     * Test of PolyRotor method, of class PolyRotor.
     */
    @Test
    public void testPolyRotor_3args() {
        System.out.println("PolyRotor");
       
        rotor.PolyRotor(0.195, 186, 75);
       
        
        assertEquals(211 , rotor.PolyRotor().xpoints[5]);   // 186 has been added to xpoints[5]     186 + 25
        assertEquals(236, rotor.PolyRotor().xpoints[4]); // 186 + 50
      
    }

    /**
     * Test of PolyRotor method, of class PolyRotor.
     */

    @Test
    public void testPolyRotor_0args() {    // does PolyRotor contain 8 npoints
        System.out.println("PolyRotor");
        //PolyRotor instance = new PolyRotor();
        //Polygon expResult = rotor.PolyRotor();
        //Polygon result = instance.PolyRotor();
        rotor.PolyRotor();
        assertEquals(8,rotor.PolyRotor().npoints); //length of xpoints and ypoints is 8

    }

    /**
     * Test of getXdot method, of class PolyRotor.
     */
    @Test
    public void testGetXdot() {
        System.out.println("getXdot");
        PolyRotor instance = new PolyRotor();
        int[] expResult = {0,25  ,25,25  ,50,25  ,25,25};
        int[] result = instance.getXdot();
        assertArrayEquals(expResult, result); //tests if xdot has the right values
    
    }

    /**
     * Test of getYdot method, of class PolyRotor.
     */
    @Test
    public void testGetYdot() {
        System.out.println("getYdot");
        PolyRotor instance = new PolyRotor();
        int[] expResult = {25,25  ,0,25  ,25,25  ,50,25};
        int[] result = instance.getYdot();
        assertArrayEquals(expResult, result);       //tests if ydot has the right values

    }




    /**
     * Test of rotate method, of class PolyRotor.
     */
    @Test
    public void testRotate() {
        rotor.PolyRotor(0.78,0,0); 
        int[] x = {0,20,20,20,40,20,20,20};
        int[] y = {20,20,0,20,20,20,40,20};
        System.out.println("rotate");
        Polygon p = new Polygon();
        for(int i = 0; i< 8; i++){
            p.addPoint(x[i], y[i]);
        }
        rotor.rotate(p);
        rotor.rotate(p);
       
        assertNotEquals(20 , rotor.PolyRotor().ypoints[4]); //tests to see if rotate changes ypoints

    }
       @Test
    public void testRotate1() {
        rotor.PolyRotor(0.78,0,0); 
        int[] x = {0,20,20,20,40,20,20,20};
        int[] y = {20,20,0,20,20,20,40,20};
        System.out.println("rotate");
        Polygon p = new Polygon();
        for(int i = 0; i< 8; i++){
            p.addPoint(x[i], y[i]);
        }
        rotor.rotate(p);
        rotor.rotate(p);
       
        assertNotEquals(40 , rotor.PolyRotor().xpoints[4]); //tests to see if rotate changes xpoints

    }

    
}
