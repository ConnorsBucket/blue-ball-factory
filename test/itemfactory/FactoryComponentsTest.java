/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package itemfactory;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.util.ArrayList;
import java.util.concurrent.locks.Lock;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author MSE-6
 */
public class FactoryComponentsTest {
    
    public FactoryComponentsTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of getMachines method, of class FactoryComponents.
     */
    @Test
    public void testGetMachines() {
        System.out.println("getMachines");
        FactoryComponents instance = new FactoryComponents();
        ArrayList<Machine> expResult = new ArrayList<>();
        ArrayList<Machine> result = instance.getMachines();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
       // fail("The test case is a prototype.");
    }

    /**
     * Test of getRotors method, of class FactoryComponents.
     */
    @Test
    public void testGetRotors() {
        System.out.println("getRotors");
        FactoryComponents instance = new FactoryComponents();
        ArrayList<PolyRotor> expResult = new ArrayList<>();
        ArrayList<PolyRotor> result = instance.getRotors();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of rotorInteraction method, of class FactoryComponents.
     */
    @Test
    public void testRotorInteraction() {
        System.out.println("rotorInteraction");
        Ball ball = new Ball(0,0,0,0);
        FactoryComponents instance = new FactoryComponents();
        instance.rotorInteraction(ball);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of removeBall method, of class FactoryComponents.
     */
    @Test
    public void testRemoveBall() {
        System.out.println("removeBall");
        Ball ball = new Ball(0,0,0,0);
        FactoryComponents instance = new FactoryComponents();
        instance.removeBall(ball);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of checkInteraction method, of class FactoryComponents.
     */
    @Test
    public void testCheckInteraction() {
        System.out.println("checkInteraction");
        Ball ball = new Ball(0,0,0,0);
        FactoryComponents instance = new FactoryComponents();
        instance.checkInteraction(ball);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }

    /**
     * Test of addRotor method, of class FactoryComponents.
     */
    @Test
    public void testAddRotor() {
        System.out.println("addRotor");
        java.util.List<PolyRotor> rotors = new ArrayList<>();
        PolyRotor b = new PolyRotor();
        
        rotors.add(b);
        rotors.add(b);
        assertEquals(1,rotors.lastIndexOf(b)); // checks if the array list index is increased
        
        
        
    }

    /**
     * Test of addBall method, of class FactoryComponents.
     */
    @Test
    public void testAddBall() {
        System.out.println("addBall");
        java.util.List<Ball> balls = new ArrayList<>();
        Ball b = new Ball(0,0,0,0);
        
        balls.add(b);
        balls.add(b);
        assertEquals(1,balls.lastIndexOf(b));  // checks if the array list index is increased
    }



    /**
     * Test of addMachine method, of class FactoryComponents.
     */
    @Test
    public void testAddMachine() {
        System.out.println("addMachine");
        Machine b = null;
        FactoryComponents instance = new FactoryComponents();
        instance.addMachine(b);
        // TODO review the generated test code and remove the default call to fail.
        //fail("The test case is a prototype.");
    }



    /**
     * Test of getPreferredSize method, of class FactoryComponents.
     */
    @Test
    public void testGetPreferredSize() {
        System.out.println("getPreferredSize");
        FactoryComponents instance = new FactoryComponents();
        Dimension expResult = new Dimension(instance.getPreferredSize());
        Dimension result = instance.getPreferredSize();
        assertEquals(expResult, result);

    }
    
}
